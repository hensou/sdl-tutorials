#include "CApp.h"

CApp::CApp()
{
	Surf_Display = NULL;
	Surf_Screen = NULL;
	Surf_Image = NULL;
	Running = true;
	Width_Display = 640;
	Height_Display = 480;
}

int CApp::OnExecute()
{
	if(OnInit() == false)
	{
		return -1;
	}

	SDL_Event Event;
	while(Running)
	{
		while(SDL_PollEvent(&Event))
		{
			OnEvent(&Event);
		}
		OnLoop();
		OnRender();
	}
	OnCleanup();
	return 0;
}

int main( int argc, char** argv )
{
	CApp theApp;
	return theApp.OnExecute();
}

