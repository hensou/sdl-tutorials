#ifndef _CAPP_H_
#define _CAPP_H_

#include<SDL2/SDL.h>
#include"CEvent.h"
#include"CSurface.h"

class CApp : public CEvent
{
	private:
		bool Running;
		SDL_Window* Surf_Display;
		SDL_Surface* Surf_Image;
		SDL_Surface* Surf_Screen;
		int Height_Display;
		int Width_Display;
	public:
		CApp();
		int OnExecute();
	public:
		bool OnInit();
		void OnEvent(SDL_Event *Event);
		void OnExit();
		void OnLoop();
		void OnRender();
		void OnCleanup();
};
#endif
