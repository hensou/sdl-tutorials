#include "CApp.h"

void CApp::OnCleanup()
{
	SDL_FreeSurface(Surf_Image);
	SDL_FreeSurface(Surf_Screen);
	SDL_DestroyWindow(Surf_Display);
	SDL_Quit();
}
