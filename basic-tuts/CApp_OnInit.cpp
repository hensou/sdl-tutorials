#include "CApp.h"

bool CApp::OnInit()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL_Init failed: %s\n", SDL_GetError());
		return false;
	}
	Surf_Display = SDL_CreateWindow("My Game Window",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			Width_Display,
			Height_Display,
			SDL_WINDOW_RESIZABLE);

	if( Surf_Display == NULL)
	{
		printf("Creating Window failed: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		Surf_Screen = SDL_GetWindowSurface(Surf_Display);
	}

	Surf_Image = CSurface::OnLoad((char *)"test.bmp");

	if ( Surf_Image == NULL )
	{
		printf("Loading Image failed: %s\n", SDL_GetError());
		return false;
	}


	return true;
}
