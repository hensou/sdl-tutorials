#include "CSurface.h"

CSurface::CSurface() {} 

SDL_Surface* CSurface::OnLoad(char* File)
{
	SDL_Surface* Surf_Temp;

	Surf_Temp = SDL_LoadBMP(File);
	if(Surf_Temp == NULL)
	{
		printf("Loading BMP Failed: %s\n", SDL_GetError());
		return NULL;
	}

	return Surf_Temp;
}

bool CSurface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int x, int y)
{
	if( Surf_Dest == NULL || Surf_Src == NULL )
	{
		printf("Surface Drawing failed: %s\n", SDL_GetError());
		return false;
	}
	else
	{

		SDL_Rect DestR;
		DestR.x = x;
		DestR.y = y;

		SDL_BlitSurface(Surf_Src, NULL, Surf_Dest, &DestR);

		return true;
	}
}

bool CSurface::OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int x, int y, int x2, int y2, int w, int h)
{
	if( Surf_Dest == NULL || Surf_Src == NULL )
	{
		printf("Surface Drawing failed: %s\n", SDL_GetError());
		return false;
	}
	else
	{

		SDL_Rect DestR;
		DestR.x = x;
		DestR.y = y;

		SDL_Rect SrcR;
		SrcR.x = x2;
		SrcR.y = y2;
		SrcR.w = w;
		SrcR.h = h;

		SDL_BlitSurface(Surf_Src, &SrcR, Surf_Dest, &DestR);

		return true;
	}
}
