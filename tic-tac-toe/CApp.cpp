#include "CApp.h"

CApp::CApp()
{

	Surf_Display = NULL;
	Surf_Screen = NULL;
	Running = true;
	Surf_Grid = NULL;
	Surf_X = NULL;
	Surf_O = NULL;
	CurrentPlayer = 0;
}

int CApp::OnExecute()
{
	if(OnInit() == false)
	{
		return -1;
	}

	SDL_Event Event;
	while(Running)
	{
		while(SDL_PollEvent(&Event))
		{
			OnEvent(&Event);
		}
		OnLoop();
		OnRender();
	}
	OnCleanup();
	return 0;
}

void CApp::Reset()
{
	for( int i = 0; i < 9; ++i )
	{
		Grid[i] = GRID_TYPE_NONE;
	}
}

void CApp::SetCell( int Id, int Type )
{
	if( Id < 0 || Id >= 9) return;
	if( Type < 0 || Type > GRID_TYPE_O ) return;

	Grid[Id] = Type;
}

void CApp::GameFinished()
{
}

int main( int argc, char** argv )
{
	CApp theApp;
	return theApp.OnExecute();
}

