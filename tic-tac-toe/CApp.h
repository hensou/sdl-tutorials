#ifndef _CAPP_H_
#define _CAPP_H_

#include<SDL2/SDL.h>
#include"CEvent.h"
#include"CSurface.h"

class CApp : public CEvent
{
	private:
		bool Running;
		SDL_Window* Surf_Display;
		SDL_Surface* Surf_Screen;
		int Height_Display;
		int Width_Display;
	private:
		SDL_Surface* Surf_Grid;
		SDL_Surface* Surf_X;
		SDL_Surface* Surf_O;
	private:
		int Grid[9];
		int CurrentPlayer;
		enum{
			GRID_TYPE_NONE = 0,
			GRID_TYPE_X,
			GRID_TYPE_O
		};

	public:
		CApp();
		int OnExecute();
	public:
		bool OnInit();
		void OnEvent(SDL_Event *Event);
		void OnKeyDown( SDL_Keycode sym, Uint16 mod, Uint16 scancode );
		void OnLButtonDown( int mx, int my );
		void OnLButtonUp( int mx, int my );
		void OnExit();
		void OnLoop();
		void OnRender();
		void OnCleanup();
		void Reset();
		void SetCell( int Id, int Type );
	public:
		void GameFinished();
};
#endif
