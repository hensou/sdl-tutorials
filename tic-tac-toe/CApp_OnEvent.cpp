#include "CApp.h"

void CApp::OnEvent(SDL_Event* Event)
{
	CEvent::OnEvent(Event);
}

void CApp::OnLButtonDown( int mx, int my )
{
	int Id = mx / 200;
	Id = Id + ( (my / 200) * 3 );

	if( Grid[Id] != GRID_TYPE_NONE )
	{
		return;
	}

	if( CurrentPlayer == 0 )
	{
		SetCell(Id, GRID_TYPE_X);
		CurrentPlayer = 1;
	}
	else
	{
		SetCell(Id, GRID_TYPE_O);
		CurrentPlayer = 0;
	}
}

void CApp::OnLButtonUp( int mx, int my )
{
	int type;
	int test = 0;
	for( int i=0; i < 9; ++i )
	{
		if( Grid[i] != GRID_TYPE_NONE )
		{
			test++;
		}
	}

	if( test == 9 )
	{
		SDL_Delay(500);
		CApp::Reset();
	}

	printf("test %d\n", test);
}

void CApp::OnKeyDown( SDL_Keycode sym, Uint16 mod, Uint16 scancode )
{
	if( sym == SDLK_BACKSPACE )
	{
		CApp::Reset();
	}
}


void CApp::OnExit()
{
	Running = false;
}
