#include "CApp.h"

bool CApp::OnInit()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL_Init failed: %s\n", SDL_GetError());
		return false;
	}
	Surf_Display = SDL_CreateWindow("tic-tac-toe",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			600,
			600,
			SDL_WINDOW_RESIZABLE);

	if( Surf_Display == NULL)
	{
		printf("Creating Window failed: %s\n", SDL_GetError());
		return false;
	}

	Surf_Screen = SDL_GetWindowSurface(Surf_Display);
	Surf_Grid = CSurface::OnLoad((char *)"./gfx/grid.bmp");
	if ( Surf_Grid == NULL )
	{
		printf("Loading Image failed: %s\n", SDL_GetError());
		return false;
	}

	Surf_X = CSurface::OnLoad((char *)"./gfx/x.bmp");
	if ( Surf_X == NULL )
	{
		printf("Loading Image failed: %s\n", SDL_GetError());
		return false;
	}
	
	Surf_O = CSurface::OnLoad((char *)"./gfx/o.bmp");
	if ( Surf_O == NULL )
	{
		printf("Loading Image failed: %s\n", SDL_GetError());
		return false;
	}

	CSurface::Transparent(Surf_X, 255, 0, 255);
	CSurface::Transparent(Surf_O, 255, 0, 255);

	Reset();

	return true;
}
