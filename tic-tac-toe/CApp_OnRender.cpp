#include "CApp.h"

void CApp::OnRender()
{
	CSurface::OnDraw(Surf_Screen, Surf_Grid, 0, 0);

	for( int i = 0; i < 9; ++i )
	{
		int x = ( i % 3 ) * 200;
		int y = ( i / 3 ) * 200;

		if ( Grid[i]  == GRID_TYPE_X )
		{
			CSurface::OnDraw(Surf_Screen, Surf_X, x, y);
		}
		else if ( Grid[i]  == GRID_TYPE_O )
		{
			CSurface::OnDraw(Surf_Screen, Surf_O, x, y);
		}

	}

	SDL_UpdateWindowSurface(Surf_Display);
}
